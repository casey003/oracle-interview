var Todos = require('../models/todoModel');
var bodyParser = require('body-parser');
var cors = require('cors');



module.exports = function(app) {

   app.use(cors({origin: 'http://129.144.188.147:3000/'}));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/api/todos', function(req, res) {

       Todos.find({}, function(err, todo) {
           if (err) throw err;

           res.send({"todos": todo});
       });

    });

   app.get('/api/todos/:id', function(req, res) {

      Todos.findById({ _id: req.params.id }, function(err, todo) {
         if (err) throw err;

         res.send(todo);
      });

   });

    app.post('/api/todos', function(req, res) {
        if (req.body.id) {
            Todos.findByIdAndUpdate(req.body.id, { todo: req.body.todo.todo, isDone: req.body.todo.isDone }, function(err, todo) {
                if (err) throw err;

                res.status(204).send("Success");
            });
        }

        else {

           var newTodo = Todos({
               todo: req.body.todo.todo,
               isDone: req.body.todo.isDone,
           });
           newTodo.save(function(err,todo) {
               if (err) throw err;

               return res.status(200).json({ todo: todo });
           });

        }

    });

    app.put('/api/todos/:id', function(req, res) {
      Todos.findByIdAndUpdate(req.params.id, { todo: req.body.todo.todo, isDone: req.body.todo.isDone }, function(err, todo) {
          if (err) throw err;
          res.status(204).send("Success");
      });
    });

    app.delete('/api/todos/:id', function(req, res) {
         Todos.findByIdAndRemove({ _id: req.params.id }, function(err) {
            if (err) throw err;
            res.status(204).send("Success");
         });

    });

    app.delete('/api/todo', function(req, res) {

        Todos.findByIdAndRemove(req.body.id, function(err) {
            if (err) throw err;
            res.status(204).send("Success");
        })

    });

}
