var Todos = require('../models/todoModel');

module.exports = function(app) {

   app.get('/api/setupTodos', function(req, res) {

       // seed database
       var starterTodos = [
           {
               todo: 'Buy milk',
               isDone: false,
           },
           {
               todo: 'Feed dog',
               isDone: false,
           },
           {
               todo: 'Learn Node',
               isDone: false,
           }
       ];
       Todos.create(starterTodos, function(err, results) {
           res.send(results);
       });
   });

}
